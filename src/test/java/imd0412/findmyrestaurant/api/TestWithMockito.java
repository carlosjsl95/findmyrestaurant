package imd0412.findmyrestaurant.api;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import imd0412.findmyrestaurant.domain.GeoCoordinate;
import imd0412.findmyrestaurant.domain.Restaurant;
import imd0412.findmyrestaurant.service.IMapService;
import imd0412.findmyrestaurant.service.IRestaurantService;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class TestWithMockito {
	@Test
	public final void testNearRestaurantsBySpeciality() {
		// INPUTS
		String speciality = "Regional";
		String address = "Av. Salgado Filho, 1900.";

		// MOCK CREATION + SPECIFICATION
		IMapService mapService = mock(IMapService.class);
		when(mapService.convertToGeographicCoordinates(any(String.class)))
				.thenReturn(new GeoCoordinate());

		IRestaurantService restaurantService = mock(IRestaurantService.class);
		when(restaurantService.findNearRestaurants(any(GeoCoordinate.class)))
				.thenReturn(buildRestaurantsList());

		// EXERCISE
		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapService,
				restaurantService);
		List<Restaurant> nearRestaurants = api.findNearRestaurantsBySpeciality(
				address, speciality);

		// CHECK
		assertFalse(nearRestaurants.isEmpty());
		verify(mapService, times(1)).convertToGeographicCoordinates(
				any(String.class));
		verify(restaurantService, times(1)).findNearRestaurants(
				any(GeoCoordinate.class));
	}

	public List<Restaurant> buildRestaurantsList() {
		List<Restaurant> restaurants = Arrays.asList(new Restaurant("RU",
				"UFRN", "Internacional"), new Restaurant("Mangai",
				"R. Antônio Basílio", "Regional"), new Restaurant("Camarões",
				"Av. Roberto Freire", "Frutos do Mar"), new Restaurant(
				"Tábua de Carne", "Av. Roberto Freire", "Regional"));

		return restaurants;
	}

}
