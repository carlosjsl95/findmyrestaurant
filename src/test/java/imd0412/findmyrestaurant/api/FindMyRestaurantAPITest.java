package imd0412.findmyrestaurant.api;

import imd0412.findmyrestaurant.domain.GeoCoordinate;
import imd0412.findmyrestaurant.domain.Restaurant;
import imd0412.findmyrestaurant.domain.Review;
import imd0412.findmyrestaurant.service.IMapService;
import imd0412.findmyrestaurant.service.IRestaurantService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Assert;

import static org.mockito.Mockito.*;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;

public class FindMyRestaurantAPITest {
	@Ignore
	@Test
	public final void testFindNearRestaurantsBySpeciality() {
		IMapService mapServiceStub = new IMapService() {

			@Override
			public GeoCoordinate convertToGeographicCoordinates(String address) {

				return new GeoCoordinate(Double.valueOf(-12.309),
						Double.valueOf(5.123));
			}
		};

		IRestaurantService restaurantService = new IRestaurantService() {

			@Override
			public List<Review> getReviews(String restaurantName) {
				throw new IllegalStateException(
						"This method cannot be invoked in this context.");
			}

			@Override
			public List<Restaurant> findNearRestaurants(
					GeoCoordinate coordinates) {
				return Arrays
						.asList(new Restaurant("Camarões",
								"Av. Roberto Freire", "Frutos do Mar"),
								new Restaurant("Restaurante Universitário",
										"Campus UFRN", "Feijoada"));
			}
		};

		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapServiceStub,
				restaurantService);

		String address = "Av. Roberto Freire";
		String speciality = "Internacional";
		List<Restaurant> restaurants = api.findNearRestaurantsBySpeciality(
				address, speciality);

		// CHECKS
		Assert.assertFalse(restaurants.isEmpty());
		Assert.assertTrue(restaurants.stream().allMatch(
				r -> r.getSpeciality().equalsIgnoreCase(speciality)));
	}

	@Test
	public final void testFindNearRestaurantsBySpeciality2() {

		// CREATE MOCKS
		IMapService mapServiceMock = Mockito.mock(IMapService.class);
		IRestaurantService restaurantServiceMock = Mockito
				.mock(IRestaurantService.class);

		// INPUT
		String speciality = "Internacional";

		GeoCoordinate myCoord = new GeoCoordinate();

		// DEFINE MOCK BEHAVIOR
		when(mapServiceMock.convertToGeographicCoordinates(anyString()))
				.thenReturn(myCoord);

		when(
				restaurantServiceMock
						.findNearRestaurants(any(GeoCoordinate.class)))
				.thenReturn(buildRestaurantsList());

		// EXERCISE THE MOCK
		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapServiceMock,
				restaurantServiceMock);

		List<Restaurant> filteredRestaurants = api
				.findNearRestaurantsBySpeciality("DummyAddress", speciality);

		// CHECKS
		Assert.assertFalse(filteredRestaurants.isEmpty());

		for (Restaurant restaurant : filteredRestaurants) {
			Assert.assertTrue(restaurant.getSpeciality().equalsIgnoreCase(
					speciality));
		}

		// VERIFICATION OF MOCK BEHAVIOR
		verify(mapServiceMock, times(1)).convertToGeographicCoordinates(
				anyString());
		verify(restaurantServiceMock, times(1)).findNearRestaurants(
				any(GeoCoordinate.class));
	}

	private List<Restaurant> buildRestaurantsList() {
		List<Restaurant> restaurants = Arrays.asList(new Restaurant("Camarões",
				"Av. Roberto Freire", "Frutos do Mar"), new Restaurant(
				"Restaurante Universitário", "Campus UFRN", "Internacional"));
		return restaurants;
	}

	@Test
	public void testGetFrequentWordsInReviews() {
		// INPUT
		String restaurantName = "";

		// CREATE MOCKS
		IMapService mapServiceMock = Mockito.mock(IMapService.class);
		IRestaurantService restaurantServiceMock = Mockito
				.mock(IRestaurantService.class);

		// DEFINE MOCK BEHAVIOR
		when(restaurantServiceMock.getReviews(restaurantName)).thenReturn(buildReviewsList());
		
		FindMyRestaurantAPI api = new FindMyRestaurantAPI(mapServiceMock,
				restaurantServiceMock);

		Map<String, Integer> frequentWordsMap = api
				.getFrequentWordsInReviews(restaurantName);
		
		Assert.assertEquals(Integer.valueOf(0), frequentWordsMap.get("restaurante"));
		Assert.assertEquals(Integer.valueOf(0), frequentWordsMap.get("ótimo,"));
	}

	private List<Review> buildReviewsList() {
		List<Review> reviews = Arrays.asList(new Review("este", new Restaurant(), Integer.valueOf(10)));
		return reviews;
	}
}
