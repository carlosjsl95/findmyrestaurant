package imd0412.findmyrestaurant.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

public class Util
{
	public static Map<String, Integer> countWords(List<String> strings)
	{
		Map<String, Integer> result = new HashMap<>();
		for (String string : strings)
		{
			String[] words = string.split(" ");
			for (String word : words)
			{
				Integer count = result.get(word);
//				BiFunction<? super Integer, ? super Integer, ? extends Integer> remappingFunction = new BiFunction<T, U, Integer>() {
//				};
//				result.merge(word, 1, remappingFunction )
				if (null == count)
				{
					result.put(word, 1);
				}
				else
				{
					result.put(word, count + 1);
				}
			}
		}
		return result;
	}
}
